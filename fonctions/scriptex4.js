function pow(x, n) {
  if (n === 0) {
    return 0
  }

  let result = x;

  for (let i = 1; i < n; i++) {
    // result = result * x
    result *= x
  }

  return result
}

// x ** n
