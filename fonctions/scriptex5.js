function pow(x, n) {
  if (n === 0) {
    return 0
  }

  let result = x;

  for (let i = 1; i < n; i++) {
    // result = result * x
    result *= x
  }

  return result
}

let x = prompt("x?", '');

if (isNaN(x)) {
  alert(`Input ${x} is not supported, use an integer!`);
}  else {
  let n = prompt("n?", '');
  
  if (isNaN(n) || +n < 1) {
    alert(`Power ${n} is not supported, use a positive integer!`);
  } else {
    alert( pow(+x, +n) );
  }
}
