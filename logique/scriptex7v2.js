const ANNEE_COURRANTE = 2022;
const LOWER_BOUND = 14;
const UPPER_BOUND = 90;

let nom = 'Gweltaz'
let annneeNaissance = prompt(`Quand es tu né ${nom}?`, 1900);

let age = +ANNEE_COURRANTE - +annneeNaissance; 
// You are 100 years old!
alert(`You are ${age} years old!`); 

if (LOWER_BOUND > age || age > UPPER_BOUND) {
    alert(`Your age is NOT between ${LOWER_BOUND} and ${UPPER_BOUND}!`); 
}
