import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SuperHero } from '../models/super-hero';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private readonly INIT_LIST: SuperHero[] = [
    {name: "Superman", alterego: "Clark Kent"},
    {name: "Robin"},
    {name: "Batgirl"}
  ]

  private list: BehaviorSubject<SuperHero[]> = new BehaviorSubject<SuperHero[]>([])

  get list$(): Observable<SuperHero[]> {
    return this.list.asObservable();
  }

  initList(): void {
    this.updateList([...this.INIT_LIST])
  }

  public updateList(superHeros: SuperHero[]): void {
    this.list.next(superHeros);
  }

  public addItem(superHero: SuperHero): void {
    const currentList = this.list.value;
    this.updateList([...currentList, superHero])
  }

  public removeItem(index: number): void {
    const currentList = this.list.value;
    currentList.splice(index, 1);
    this.updateList(currentList)
  }
}
