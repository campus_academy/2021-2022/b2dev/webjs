import { Component } from '@angular/core';

@Component({
  selector: 'app-test-directives',
  template: `
    <div [hidden]="hidden" [ngStyle]="{'color': color, 'font-size': size+'px'}">
      style using ngStyle
    </div>

    <div [ngClass]="{'bold': true}">
      style using ngClass
    </div>

    <input [(ngModel)]="color" />
    <button (click)="size = size + 1">+</button>
    <button (click)="size = size - 1">-</button>
    <button (click)="hidden = !hidden">toogle hidden</button>

  `,
  styles: [
    `    .bold {
      font-weight: bold
    }`
  ]
})
export class TestDirectivesComponent {
  color = 'red';
  size = 12;
  hidden = true;
}
