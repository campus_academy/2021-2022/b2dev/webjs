import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SuperHero } from 'src/app/models/super-hero';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-action-bar',
  template: `
    <form [formGroup]="form" (ngSubmit)="createHero()">
      <input type="text" formControlName="name">
      <input type="text" formControlName="alterego">
      <button type="submit" [disabled]="!isFormValid()">
        Add new super-hero
      </button>
    </form>

    <button (click)="initList()">Reinit the list</button>
  `,
})
export class ActionBarComponent {
  form: FormGroup = this.fb.group({
    name: this.fb.control('', Validators.required),
    alterego: this.fb.control('')
  });
  superHeroName!: string;
  superHeroCivilName!: string;

  constructor(
    private listService: ListService,
    private fb: FormBuilder
  ) {
  }

  isFormValid(): boolean {
    return this.form.valid && !this.form.pristine;
  }

  createHero(): void {
    this.listService.addItem(this.form.value as SuperHero);
  }

  initList(): void {
    this.listService.initList()
  }
}
