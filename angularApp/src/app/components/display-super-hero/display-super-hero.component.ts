import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SuperHero } from 'src/app/models/super-hero';

@Component({
  selector: 'app-display-super-hero',
  template: `
    {{superHero.name}}
    [
    <span *ngIf="hasAlterego(); else noIdentity">
      {{superHero.alterego}}
    </span>
    ]
    <button (click)="removeItem()"> Remove </button>

    <ng-template #noIdentity>
      <span>
        <em> Identité civile inconnue </em>
      </span>
    </ng-template>
  `
})
export class DisplaySuperHeroComponent {
  @Input() superHero!: SuperHero;
  @Output() remove: EventEmitter<void> = new EventEmitter();

  removeItem() {
    this.remove.emit()
  }

  hasAlterego(): boolean {
    return this.superHero.alterego !== undefined && this.superHero.alterego !== ''
  }
}
