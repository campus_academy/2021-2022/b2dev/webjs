import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SuperHero } from 'src/app/models/super-hero';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-display-list-super-heros',
  template: `
    <ng-container *ngIf="(list$ | async) as list">
      <div *ngIf="list.length > 0; else noSuperHero">
        <h2>Super-Heros List</h2>
        <ul>
          <li *ngFor="let superHero of list; index as i">
            <app-display-super-hero
              [superHero]="superHero"
              (remove)="removeItem(i)">
            </app-display-super-hero>
          </li>
        </ul>
      </div>

      <ng-template #noSuperHero>
        <div>
          No Super-Heros yet ! feel fee to add one :D
        </div>
      </ng-template>
    </ng-container>
  `
})
export class DisplayListSuperHerosComponent {
  list$: Observable<SuperHero[]> = this.listService.list$

  constructor(
    private listService: ListService
  ) {

  }

  removeItem(i: number) {
    this.listService.removeItem(i);
  }
}
