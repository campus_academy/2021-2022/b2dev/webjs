import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ListService } from './services/list.service';

@Component({
  selector: 'app-root',
  template: `
    <h1>Bienvenue sur mon super site !!!! {{date$ | async | date:"dd/MM/yyyy hh:mm:ss"}} </h1>

  <app-action-bar></app-action-bar>

  <app-display-list-super-heros> </app-display-list-super-heros>
  `,
  styles: [`
    h1 {
      color: red
    }
  `]
})
export class AppComponent implements OnInit  {

  private date: BehaviorSubject<Date> = new BehaviorSubject<Date>(new Date());
  date$: Observable<Date> = this.date.asObservable();

  list$ = this.listService.list$

  constructor(
    private listService: ListService
  ) {
  }

  ngOnInit(): void {
    setInterval(() => this.date.next(new Date()), 1000);
    this.listService.initList()
  }
}
