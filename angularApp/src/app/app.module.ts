import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ActionBarComponent } from './components/action-bar/action-bar.component';
import { DisplayListSuperHerosComponent } from './components/display-list-super-heros/display-list-super-heros.component';
import { DisplaySuperHeroComponent } from './components/display-super-hero/display-super-hero.component';
import { TestDirectivesComponent } from './components/test-directives/test-directives.component';

@NgModule({
  declarations: [
    AppComponent,
    DisplaySuperHeroComponent,
    DisplayListSuperHerosComponent,
    ActionBarComponent,
    TestDirectivesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
