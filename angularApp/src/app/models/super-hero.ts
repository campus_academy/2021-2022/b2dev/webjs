export class SuperHero {
  constructor(
    public name: string,
    public alterego?: string
  ) {
  }
}
