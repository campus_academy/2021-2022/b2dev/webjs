# AngularApp

## Ajouter un component
*nom*: ActionBarCompenent

*contenu*: les 2 inputs et les 2 boutons

*action*: permetre d'ajouter un hero et de reinit la liste

*steps*: 
- créer le component `ng g c ActionBar --inline-template --style none` 
- déplacer le html dans le component
``` HTML
    <input type="text" [(ngModel)]="superHeroName">
    <input type="text" [(ngModel)]="superHeroCivilName">
    <button [disabled]="isButtonDisabled()" (click)="createHero()">
      Add new super-hero
    </button>
    <button (click)="initList()">Reinit the list</button>
```
- ajouter les méthodes/propriétés qui manque
``` TS
superHeroName!: string;
  superHeroCivilName!: string;

  isButtonDisabled(): boolean {
    return this.superHeroName === undefined || this.superHeroName === "";
  }

  createHero(): void {

  }

  initList(): void {

  }
```
- mettre le nouveau composant dans le parent 
``` HTML
<app-action-bar></app-action-bar>
```
- ajouter l'ouput pour la creation du héros
  - ajouter la propriété @Output() dans le composant
  ``` TS
    @Output() create: EventEmitter<SuperHero> = new EventEmitter();
  ```
  - ajouter la methode pour "emit" la nouvelle donnée
  ```ts
    createHero(): void {
      const superHero = new SuperHero(this.superHeroName, this.superHeroCivilName);
    this.create.emit(superHero);
  }
  ```
  - ajouter l'event binding dans le composant parent
  ``` HTML
  <app-action-bar (create)="addNewItem($event)"></app-action-bar>
  ```
- ajouter l'ouput pour reinit la list
  - ajouter la propriété @Output() dans le composant
  ``` TS
    @Output() reinit: EventEmitter<any> = new EventEmitter();
  ```
  - ajouter la methode pour "emit" l'event (pas de donnée)
  ``` TS
    initList(): void {
      this.reinit.emit()
    }
  ```
  - ajouter l'event binding dans le composant parent
  ``` HTML
  <app-action-bar
    (create)="addNewItem($event)"
    (reinit)="initList()"
  ></app-action-bar>
  ```

## Utiliser *ngIf

Afficher l'alterego d'un hero si il est présent dans l'objet
sinon afficher "identité inconnue"

## Ajouter une date mise a jours toute les secondes en utilisant un subject/observables

Steps: 
- créez une propriété date dans le composant
- utiliser un observable et le pipe async pour l'afficher dans le titre <h1>
- mettre en place un mechanisme pour la mettre au click sur un bouton
----
- changer le mechanisme de mise a jours pour qu'elle soit faite toutes les secondes
----
- change le format de la date pour celui-ci "06/01/2022 14:21:56"
