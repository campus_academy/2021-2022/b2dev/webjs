
document.addEventListener("DOMContentLoaded",() => {
    initList()
});

function initList() {
    ["Robin", "Superman"].forEach(element => createListElement(element))
}

function addNewItem() {
    createListElement(document.getElementById("superHeroNameInput").value)
}

function createListElement(elementText) {
    let ul = document.getElementById("list");
    let li = document.createElement("li");

    li.appendChild(document.createTextNode(elementText));
    li.appendChild(getRemoveButton(li))
    ul.appendChild(li);
}

function remove(node) {
    let ul = document.getElementById("list");
    ul.removeChild(node)
}

function getRemoveButton(target) {
    let b = document.createElement("button");
    b.innerText = 'remove';
    b.addEventListener("click", () => remove(target));
    b.classList.add("ml-1");

    return b;
}

function reinitList() {
    let ul = document.getElementById("list");
    ul.replaceChildren();

    initList()
}