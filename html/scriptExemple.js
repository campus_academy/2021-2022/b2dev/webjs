function addSpan() {
    let srcSpan =  document.getElementById('txtSrc');
    let srcSpanValue = srcSpan.innerHTML
    // let srcInputValue = srcInput.value
    let div = document.getElementById('content-wrapper');
    let span = document.createElement('span');
    span.innerHTML = `<strong>${srcSpanValue}</strong>`
    // span.textContent = "<strong>${srcSpanValue}</strong>"
    div.appendChild(span)
}

let table = ["test",2,true,"test",6,{num: 8},6,9,(text) => {
    console.log(text)
}]

document.addEventListener("DOMContentLoaded",() => {
    console.log('DOMContentLoaded')
    console.log(table.length)
    table.forEach(function toto(element, index) {
        console.log(element)
        if (index === 8) {
            element("element")
            table[8]("table")
        }
    })
    table.forEach((element, index) => {
        console.log(`${index} => ${element}`)
    })
});