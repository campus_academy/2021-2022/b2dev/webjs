const LIST_INIT = ["Superman", "Robin", "Batgirl"];

document.addEventListener("DOMContentLoaded", () => {
    initList();
});

function initList() {
    LIST_INIT.forEach((element, index) => {
        addListElement(element, index)
    });   
     
    // LIST_INIT.forEach(createListElement)
}

function addNewItem() {
    addListElement(document.getElementById("superHeroNameInput").value)
}

function addListElement(elementText, index) {
    console.log(`adding: ${elementText} [${index}]`)
    let ul = document.getElementById("list");
    ul.appendChild(createLiElement(elementText));

    // ul.innerHTML += `<li>${elementText}</li>`;
}

function createLiElement(elementText) {
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(elementText));
    li.classList.add("list-group-item");
    li.appendChild(createButtonElement(li))
    return li;
} 

function createButtonElement(li) {
    let button = document.createElement("button");
    button.appendChild(document.createTextNode("remove"));
    button.classList.add("ml-1");
    button.classList.add("btn");
    button.classList.add("btn-danger");
    button.addEventListener("click", () => {
        removeLiFromList(li)
    });
    return button;
}

function removeLiFromList(li) {
    let ul = document.getElementById("list");
    ul.removeChild(li)
}

function reinitList() {
    let ul = document.getElementById("list");
    // ul.replaceChildren();
    ul.innerHTML = ""
    initList();
}