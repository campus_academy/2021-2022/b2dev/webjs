let counter;

function initCounter() {
    counter = 10;
    setCounter();
}

function increment() {
    counter++;
    setCounter();
}

function decrement() {
    counter--;
    setCounter();
}

function resetCounter() {
    counter = 0;
    setCounter();
}

function setCounter() {
    document.getElementById('displayCounter').innerHTML = counter;
}
