const MSG_CANCELED = 'Canceled';
const MSG_WELCOME = 'Welcome!';
const MSG_ASK_LOGIN = 'Who\'s there?';
const MSG_ASK_PASSWORD = 'password?';
const MSG_ERROR_LOGIN =  'I don\'t know you';
const MSG_ERROR_PASSWORD =  'Wrong password';
const ADMIN_LOGIN = 'Admin';
const ADMIN_PASSWORD = 'TheMaster';

let userName = prompt(MSG_ASK_LOGIN, '');

if (userName === ADMIN_LOGIN) {

  let password = prompt(MSG_ASK_PASSWORD, '');

  if (password === ADMIN_PASSWORD) {
    alert( MSG_WELCOME );
  } else if (password === '' || password === null) {
    alert( MSG_CANCELED );
  } else {
    alert( MSG_ERROR_PASSWORD );
  }

} else if (userName === '' || userName === null) {
  alert( MSG_CANCELED );
} else {
  alert( MSG_ERROR_LOGIN );
}